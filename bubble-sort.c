#include <stdio.h>
#include <stdlib.h>

void sort(int *array, size_t arraySize)
{
	for (size_t i = 0; i < arraySize - 1; i++) {
		for (size_t j = 0; j < arraySize - 1 - i; j++) {
			if (array[j] > array[j + 1]) {
				int temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
}

int main(void)
{
	size_t arraySize = 100;
	int array[arraySize];
	for (size_t i = 0; i < arraySize; i++) {
		array[i] = rand() % 100;
	}
	for (size_t i = 0; i < arraySize; i++) {
		printf("%d ", array[i]);
	}
	printf("\n");
	sort(array, arraySize);
	for (size_t i = 0; i < arraySize; i++) {
		printf("%d ", array[i]);
	}
}
