#include <stdio.h>
#include <stdlib.h>

void sort(int *array, size_t arraySize)
{
	for (size_t i = 1; i < arraySize; i++) {
		int key = array[i];
		size_t j = i;
		while(j > 0) {
			if (key < array[j - 1]) {
				array[j] = array[j - 1];
				j--;
			}
		}
		array[j] = key;
	}
}

int main(void)
{
	size_t arraySize = 100;
	int array[arraySize];
	for (size_t i = 0; i < arraySize; i++) {
		array[i] = rand() % 100;
	}
	for (size_t i = 0; i < arraySize; i++) {
		printf("%d ", array[i]);
	}
	printf("\n");
	sort(array, arraySize);
	for (size_t i = 0; i < arraySize; i++) {
		printf("%d ", array[i]);
	}
}
