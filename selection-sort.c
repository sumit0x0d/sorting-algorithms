#include <stdio.h>
#include <stdlib.h>

void sort(int *array, size_t arraySize)
{
	for (size_t i = 0; i < arraySize - 1; i++) {
		size_t k = i;
		for (size_t j = i; j < arraySize; j++) {
			if (array[j] < array[k]) {
				k = j;
			}
		}
		int temp = array[i];
		array[i] = array[k];
		array[k] = temp;
	}
}

int main(void)
{
	size_t arraySize = 100;
	int array[arraySize];
	for (size_t i = 0; i < arraySize; i++) {
		array[i] = rand() % 100;
	}
	for (size_t i = 0; i < arraySize; i++) {
		printf("%d ", array[i]);
	}
	printf("\n");
	sort(array, arraySize);
	for (size_t i = 0; i < arraySize; i++) {
		printf("%d ", array[i]);
	}
}
